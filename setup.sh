#!/bin/bash
wget http://hepmc.web.cern.ch/hepmc/releases/hepmc2.06.09.tgz
tar xzf hepmc2.06.09.tgz
cd hepmc2.06.09
./configure --prefix=${PWD}/installed --with-momentum=GEV --with-length=MM
make -j6; make install

cd ..
wget https://lhapdf.hepforge.org/downloads?f=LHAPDF-6.2.3.tar.gz
tar xf downloads?f=LHAPDF-6.2.3.tar.gz
cd LHAPDF-6.2.3
./configure --prefix=${PWD}/installed/
make -j6; make install

cd ..
wget http://home.thep.lu.se/~torbjorn/pythia8/pythia8244.tgz
tar xzf pythia8244.tgz
cd pythia8244/
./configure --with-hepmc2=${PWD}/../hepmc2.06.09/installed --with-lhapdf6=${PWD}/../LHAPDF-6.2.3/installed
make -j6; make install

cd examples
make main42
#cp ~/public/neutrino-tungsten.cmnd .
#./main42 neutrino-tungsten.cmnd out42

