#To get Pythia to produce the tau suppression plots:

#Give permissions to the setup.sh script:

chmod 755 setup.sh

#Run the script to setup LHAPDF-6.2.3, HepMC2, and Pythia

./setup.sh

#Copy the tungsten nuclear PDF from https://lhapdf.hepforge.org/downloads/?f=pdfsets/v6.backup/current into LHAPDF-6.2.3/include/LHAPDF/

#Copy crosssection.cc, crosssectiontau.cc, scan.sh, and write.py into pythia8244/examples/

#Compile crosssection.cc and crosssectiontau.cc:

g++ crosssection.cc -o crosssection -I/eos/user/j/jwspence/pythia-install/pythia8244/../hepmc2.06.09/installed/include -I../include -O2  -ped antic -W -Wall -Wshadow -fPIC -L../lib -Wl,-rpath,../lib -lpythia8 -ldl -L/eos/user/j/jwspence/pythia-install/pythia8244/../hepmc2.06.09/installed/lib -Wl,-rpath,/eos/ user/j/jwspence/pythia-install/pythia8244/../hepmc2.06.09/installed/lib -lHepMC

g++ crosssectiontau.cc -o crosssectiontau -I/eos/user/j/jwspence/pythia-install/pythia8244/../hepmc2.06.09/installed/include -I../include -O2  -ped antic -W -Wall -Wshadow -fPIC -L../lib -Wl,-rpath,../lib -lpythia8 -ldl -L/eos/user/j/jwspence/pythia-install/pythia8244/../hepmc2.06.09/installed/lib -Wl,-rpath,/eos/ user/j/jwspence/pythia-install/pythia8244/../hepmc2.06.09/installed/lib -lHepMC

#Give permissions to the scan.sh script:

chmod 755 scan.sh

#Run the script to scan over the energies:

./scan.sh

#Write the cross-sections and uncertainties to files:

python write.py

#Read in the four columns separately and make a TMultiGraph:

root TauSuppression.cpp
