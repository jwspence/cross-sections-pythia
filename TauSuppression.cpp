#include <cmath>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>

void TauSuppression(){
     using namespace std;
    	ifstream in("nu_tau_suppression_energies.txt", ifstream::in);
    	Double_t energy[41];
        int i = 0;
    	while((!in.eof()) && in)
    		{
    		float iEnergy = 0;
                in >> iEnergy;
    		energy[i] = iEnergy;
                i += 1;
    		}
    	in.close();
        in.open("Sigma.txt",ifstream::in);
        Double_t sigma[41];
        i = 0;
        while ((!in.eof()) && in)
                 {
                 float iSigma = 0;
                 in >> iSigma;
                 sigma[i] = iSigma;
                 i += 1;
                 }
        in.close();
        in.open("SigmaTau.txt",ifstream::in);
        Double_t sigmatau[41];
        i = 0;
        while ((!in.eof()) && in)
                 {
                 float iSigmaTau = 0;
                 in >> iSigmaTau;
                 sigmatau[i] = iSigmaTau;
                 i += 1;
                 }
        in.close();
        in.open("DeltaSigma.txt");
        Double_t deltasigma[41];
        i = 0;
        while((!in.eof()) && in)
                {
                float iDeltaSigma = 0;
                in >> iDeltaSigma;
                deltasigma[i] = iDeltaSigma;
                i += 1;
                }
        in.close();
        in.open("DeltaSigmaTau.txt");
        Double_t deltasigmatau[41];
        i = 0;
        while((!in.eof()) && in)
                {
                float iDeltaSigmaTau = 0;
                in >> iDeltaSigmaTau;
                deltasigmatau[i] = iDeltaSigmaTau;
                i += 1;
                }
        in.close();
        in.open("nu_tau_suppression_factor.txt");
        Double_t refsupp[41];
        i = 0;
        while((!in.eof()) && in)
                {
                float iSupp = 0;
                in >> iSupp;
                refsupp[i] = iSupp;
                i += 1;
                }
        in.close();
   gSystem->Load("LHAPDF-6.2.3/installed/lib/libLHAPDF.so");
   TCanvas *c1 = new TCanvas("c1","Nu tau suppression",10,1000,200,600);
   c1->SetLogx();
//   c1->SetLogy();
   Int_t n = 40;
   double_t deltaenergy[41],supp[41],deltasupp[41];
   for (Int_t j=0;j<n;j++) {
   deltaenergy[j] = 0;
   supp[j] = sigmatau[j]/sigma[j];
   deltasupp[j] = supp[j]*pow(pow(deltasigma[j]/sigma[j],2)+pow(deltasigmatau[j]/sigmatau[j],2),.5);
//   x[j] = 10*pow(1000,j/100.);
//   z[j] = xs[j]/10/pow(1000,j/100.);
   }
   for (Int_t i=0;i<n;i++) {
//   x[i] = 10*pow(1000,i/100.);
//   y[i] = sigma(energy[i],1,1)*supp[i]/energy[i];
     cout << energy[i] << " " << sigma[i] << " " << deltasigma[i] << " " << sigmatau[i] << " " << deltasigmatau[i] << " " << endl;
   }
   TGraphErrors* gr = new TGraphErrors(n,energy,supp,deltaenergy,deltasupp);
   TGraph* cmp = new TGraph(n,energy,refsupp);
   gr->SetTitle("Pythia simulation");
   cmp->SetTitle("Reference suppression factor");
   cmp->SetLineColor(4);
//   gr->Draw("AC*");
   TMultiGraph *mg = new TMultiGraph();
   mg->Add(gr);
   mg->Add(cmp);
   mg->Draw("a");
   mg->SetTitle("Nu Tau Suppression");
   mg->GetXaxis()->SetTitle("Energy (GeV)");
   mg->GetYaxis()->SetTitle("Nu Tau Suppression Ratio");
   c1->BuildLegend();
}
